import { Given, Then, When } from 'cucumber';
import { demoPage } from '../pages/Demo.page';

Given(/^I'm on the demo page$/, () => {
    demoPage.open();
});

When(/^I calculate sum assured by premium:$/, (data) => {
    const [params] = data.hashes();
    demoPage.calculate(params);
});

Then(/^Show list quotation product base on annual premium$/, () => {
    expect(demoPage.tableRowCount).toBeGreaterThan(0);
});

When(/^I calculate premium by sum assured:$/, (data) => {
    const [params] = data.hashes();
    demoPage.calculate(params);
});

Then(/^Show list quotation product base on sum assured$/, () => {
    expect(demoPage.tableRowCount).toBeGreaterThan(0);
});
