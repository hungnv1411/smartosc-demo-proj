class DemoPage {
    open() {
        browser.url(browser.options.baseUrl + '/demo/calculation');
    }

    get tableRowCount() {
        return $$('.MuiTableRow-root').length;
    }

    calculate(params: {
        name: string;
        genderCd: string;
        dob: string;
        planCode: string;
        paymentFrequency?: string;
        premiumPerYear?: number;
        saPerYear?: number;
    }) {
        $('[name=username]').setValue(params.name);

        $('#gender').parentElement().$('[title="Open"]').click();
        browser.pause(300);
        $('#gender-option-0').click();

        $('[name=dob]').setValue(params.dob);

        $('#planCode').parentElement().$('[title="Open"]').click();
        browser.pause(300);
        $('#planCode-option-0').click();

        $('#paymentFrequency').parentElement().$('[title="Open"]').click();
        browser.pause(300);
        $('#paymentFrequency-option-0').click();

        if (params.premiumPerYear !== undefined) {
            $('[name=calculateMethod]').parentElement().click();
            browser.pause(300);
            $('#menu-calculateMethod').$('[data-value="0"]').click();
            $('[name=premiumPerYear]').setValue(params.premiumPerYear || 0);
        } else if (params.saPerYear !== undefined) {
            $('[name=calculateMethod]').parentElement().click();
            browser.pause(300);
            $('#menu-calculateMethod').$('[data-value="1"]').click();
            $('[name=sumAssuredPerYear]').setValue(params.saPerYear || 0);
        }

        $('[name=calculateButton]').click();
        browser.pause(5000);
    }
}

export const demoPage = new DemoPage();
