Feature: Performing insurance calculation

    Scenario: Calculate sum assured by premium
        Given I'm on the demo page
        When I calculate sum assured by premium:
            | name | genderCd | dob         | planCode  | paymentFrequency  | premiumPerYear    |
            | Test | FEMALE   | 1983/02/21  | T11A20    | YEARLY            | 30000             |
        Then Show list quotation product base on annual premium

    Scenario: Calculate premium by sum assured
        Given I'm on the demo page
        When I calculate premium by sum assured:
            | name | genderCd | dob         | planCode  | paymentFrequency  | saPerYear    |
            | Test | FEMALE   | 1983/02/21  | T11A20    | YEARLY            | 30000             |
        Then Show list quotation product base on sum assured
