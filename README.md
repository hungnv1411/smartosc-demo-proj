## Demo for insurance calculation

- [Webapp](webapp/README.md)
- [Backend](bff-server/README.md)
- [UI Automation test](ui-automation-test/README.md)

## Screenshots

### Fullpage

<img src="webapp/public/demo-web.png" width="700"/>

### Mobile responsive

<img src="webapp/public/demo-mobile.png" width="350"/>
