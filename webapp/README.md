# Demo

## Setup

- Nodejs
- Yarn

```sh
npm install -g yarn

```

## Run local

```sh
yarn start

```

## Cli

- Generate slice

```sh
npm run generate slice slice_name

```

- Generate component

```sh
npm run generate component ComponentName

```

## Run unit test

```sh
yarn run test
```

<pre>
PASS  src/app/pages/DemoPage/slice/__tests__/slice.test.ts
PASS  src/app/pages/DemoPage/slice/__tests__/saga.test.ts
PASS  src/store/__tests__/reducer.test.ts
PASS  src/app/pages/DemoPage/slice/__tests__/selectors.test.ts
PASS  src/store/__tests__/configureStore.test.ts
PASS  src/styles/__tests__/media.test.ts
PASS  src/locales/__tests__/i18n.test.ts
PASS  src/app/pages/DemoPage/SearchBar/__tests__/index.test.tsx
PASS  src/app/pages/DemoPage/__tests__/index.test.tsx
PASS  src/app/__tests__/index.test.tsx

Test Suites: 10 passed, 10 total
Tests:       18 passed, 18 total
Snapshots:   3 passed, 3 total
Time:        7.25 s
Ran all test suites.
</pre>
