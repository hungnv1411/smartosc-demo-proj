export const currencyFormat = (str, unit = '$') =>
  `${String(str).replace(/(.)(?=(\d{3})+$)/g, '$1,')} ${unit}`;
