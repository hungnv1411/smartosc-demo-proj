import forEach from 'lodash/forEach';
import isEmpty from 'lodash/isEmpty';
import isNaN from 'lodash/isNaN';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';
import trim from 'lodash/trim';

const isEmail = (email?: string) => {
  if (email) {
    const pattern = new RegExp(
      // tslint:disable-next-line: max-line-length
      /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i,
    );
    return pattern.test(email);
  }
  return false;
};

const isEmptyValue = value => {
  if (Number.isInteger(value)) {
    return isNaN(value) || isUndefined(value) || value === 0;
  }
  return (
    isEmpty(value) ||
    isUndefined(value) ||
    isNull(value) ||
    isNaN(value) ||
    trim(`${value}`).length === 0
  );
};

const emptyValidate = (
  validate,
  object,
  keys: string[],
  messages?: string[],
) => {
  const result = { ...validate };
  keys.forEach((key, index) => {
    if (
      (isEmpty(validate) || isUndefined(validate[key])) &&
      (isUndefined(object[key]) || isEmptyValue(object[key]))
    ) {
      result[key] = {
        error: true,
        helperText: `${key}IsRequired`,
        helperMessageText:
          messages && messages.length > index ? messages[index] : '',
      };
    }
  });
  return result;
};

const emailValidate = (
  validate,
  object,
  keys: string[],
  messages?: string[],
) => {
  const result = { ...validate };
  keys.forEach((key, index) => {
    if (
      (isEmpty(validate) || isUndefined(validate[key])) &&
      (isUndefined(object[key]) || !isEmail(`${object[key]}`.trim()))
    ) {
      result[key] = {
        error: true,
        helperText: `${key}IsInvalid`,
        helperMessageText:
          messages && messages.length > index ? messages[index] : '',
      };
    }
  });
  return result;
};

const validation = (
  payload: Array<{
    type: string;
    model: any;
    keys: string[];
    messages?: string[];
  }>,
) => {
  let result;
  forEach(payload, validate => {
    const { type, model, keys, messages } = validate;
    if (type === 'empty') {
      result = emptyValidate(result, model, keys, messages);
    } else if (type === 'email') {
      result = emailValidate(result, model, keys, messages);
    }
  });
  return result;
};

const emailValidation = isEmail;

const phoneValidation = (phone: string) => {
  const re = /^[0-9]*$/g;
  return re.test(phone);
};

export interface ValidationResult {
  error: boolean;
  helperText?: string;
  helperMessageText?: string;
}

export default validation;
export { emailValidation, phoneValidation };
