import { DemoPage } from 'app/pages/DemoPage/Loadable';
import { HomePage } from 'app/pages/HomePage/Loadable';
import { Layout } from 'app/pages/Layout';
import { NotFoundPage } from 'app/pages/NotFoundPage/Loadable';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { paths } from './paths';

const FullRoutes = () => (
  <Switch>
    <Redirect exact from="/" to={paths.demo} />
    <Route exact path={paths.notFound} component={NotFoundPage} />
    <Layout>
      <Switch>
        <Route exact path={paths.home} component={HomePage} />
        <Route exact path={paths.demo} component={DemoPage} />
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </Layout>
  </Switch>
);

export default FullRoutes;
