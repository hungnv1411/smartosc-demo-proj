import { generatePath } from 'react-router';

const paths = {
  home: '/welcome',
  notFound: '/not-found',
  demo: '/demo/calculation',
};

const createPath = (pattern: string, params: any) =>
  generatePath(pattern, params);

export { paths, createPath };
