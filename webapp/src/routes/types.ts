export interface IRoute {
  id: string; // !MUST localize in components/Sidebar/messages.ts
  authenticationRequired?: boolean; // default TRUE
  path?: string;
  component?: any;
}

export interface ISidebarSubMenu extends IRoute {
  visible?: boolean; // default FALSE
}

export interface ISidebarMenu extends IRoute {
  icon?: any;
  subMenus: ISidebarSubMenu[];
}
