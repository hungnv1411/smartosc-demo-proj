import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import get from 'lodash/get';
import { paths } from 'routes/paths';
declare module 'axios' {
  export interface AxiosRequestConfig {
    throwAccessDenied?: boolean; // is true if you want to self handle access denied exception
  }
}

export const createService = (
  baseURL?: string,
  contentType: string = 'application/json',
): AxiosInstance => {
  return interceptAuth(baseConfig(baseURL, contentType));
};

export const downloadFileService = (
  baseURL?: string,
  contentType: string = 'application/json',
): AxiosInstance => {
  const config: AxiosRequestConfig = baseConfig(baseURL, contentType);
  config.responseType = 'blob';
  return interceptAuth(config);
};

const baseConfig = (
  baseURL?: string,
  contentType: string = 'application/json',
) => {
  return {
    baseURL: baseURL,
    headers: {
      'Accept-Language': 'en-US',
      'Content-Type': contentType,
    },
  };
};

const interceptAuth = (config: AxiosRequestConfig) => {
  const instance = axios.create(config);
  instance.interceptors.request.use(config => {
    // const token = 'TODO';
    // if (token) {
    //   config.headers['Authorization'] = 'bearer ' + token;
    // }
    return config;
  });
  instance.interceptors.response.use(
    response => {
      // Do something with response data
      return response;
    },
    error => {
      const statusCode = get(error, 'response.status');
      const errorMessage = get(error, 'response.data.error');
      const providerUrl =
        window.location.protocol +
        '//' +
        window.location.hostname +
        (window.location.port ? ':' + window.location.port : '');
      if (statusCode === 401 && errorMessage === 'invalid_token') {
        // LocalStorage.removeAllItem();
        window.location.href = `${providerUrl}${paths.home}`;
      } else if (statusCode === 403) {
        const throwAccessDenied: boolean = get(
          error,
          'config.throwAccessDenied',
          false,
        );
        if (!throwAccessDenied) {
          // window.location.href = `${providerUrl}${path.accessDenied}`;
        }
      }
      // Do something with response error
      return Promise.reject(error);
    },
  );
  return instance;
};

export const createServiceNoToken = (baseURL?: string): AxiosInstance => {
  const instance = axios.create({
    baseURL: baseURL,
    headers: {
      'Accept-Language': 'en-US',
      'Content-Type': 'application/json',
    },
  });
  instance.interceptors.request.use(config => {
    return config;
  });
  return instance;
};
