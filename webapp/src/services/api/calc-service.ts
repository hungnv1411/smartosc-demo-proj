import { InsuranceCalcParameters } from 'app/pages/DemoPage/slice/types';
import { AxiosResponse } from 'axios';
import { QuotationProduct } from 'models';
import { createService } from './axios';

const baseURL = process.env.REACT_APP_PRODUCTS_API;
const instance = createService(baseURL);

async function getProduct(request: InsuranceCalcParameters) {
  const body = request;
  const response: AxiosResponse<{
    data: QuotationProduct[];
    message: string;
  }> = await instance.post('/getProduct', body);
  return response.data.data;
}

const service = {
  getProduct,
};

export default service;
