import { PaymentFrequency } from './quotation-product';

export const genderOptions = [
  { label: 'Male', value: 'MALE' },
  { label: 'Female', value: 'FEMALE' },
];

export const plans = [
  {
    label: 'Package 1 (benefit 200k)',
    value: 'T11A20',
  },
  {
    label: 'Package 2 (benefit 500k)',
    value: 'T11A50',
  },
  {
    label: 'Package 3 (benefit 1M)',
    value: 'T11AM1',
  },
];

export const paymentFrequency = [
  {
    label: 'Yearly',
    value: PaymentFrequency.YEARLY,
  },
  {
    label: 'Half Yearly',
    value: PaymentFrequency.HALFYEARLY,
  },
  {
    label: 'Quarterly',
    value: PaymentFrequency.QUARTERLY,
  },
  {
    label: 'Monthly',
    value: PaymentFrequency.MONTHLY,
  },
];
