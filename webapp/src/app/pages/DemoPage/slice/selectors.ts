import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from '.';

const selectSlice = (state: RootState) => state.insuranceCalc || initialState;

export const selectInsuranceCalc = createSelector(
  [selectSlice],
  state => state,
);

export const selectQuotationProducts = createSelector(
  [selectSlice],
  state => state.quotationProducts,
);
