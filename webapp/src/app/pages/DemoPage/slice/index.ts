import { PayloadAction } from '@reduxjs/toolkit';
import { QuotationProduct } from 'models';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { insuranceCalcSaga } from './saga';
import { InsuranceCalcParameters, InsuranceCalcState } from './types';

export const initialState: InsuranceCalcState = {
  quotationProducts: [],
};

const slice = createSlice({
  name: 'insuranceCalc',
  initialState,
  reducers: {
    getProductsAction(state, action: PayloadAction<InsuranceCalcParameters>) {},
    getProductsSuccess(state, action: PayloadAction<QuotationProduct[]>) {
      state.quotationProducts = action.payload;
    },
    getProductsFailed(state) {},
  },
});

export const { actions: insuranceCalcActions, reducer } = slice;

export const useInsuranceCalcSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: insuranceCalcSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useInsuranceCalcSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
