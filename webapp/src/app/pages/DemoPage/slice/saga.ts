import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, takeLatest } from 'redux-saga/effects';
import { CalculationService } from 'services';
import { insuranceCalcActions as actions } from '.';
import { InsuranceCalcParameters } from './types';

export function* getProducts(action: PayloadAction<InsuranceCalcParameters>) {
  const payload = action.payload;
  try {
    const result = yield call(CalculationService.getProduct, payload);
    if (result) {
      yield put(actions.getProductsSuccess(result));
    } else {
      yield put(actions.getProductsFailed());
    }
  } catch (err) {
    yield put(actions.getProductsFailed());
  }
}

export function* insuranceCalcSaga() {
  yield takeLatest(actions.getProductsAction.type, getProducts);
}
