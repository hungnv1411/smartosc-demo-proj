import { RootState } from 'types';
import { initialState } from '..';
import * as selectors from '../selectors';

describe('DemoPage selectors', () => {
  let state: RootState = {};

  beforeEach(() => {
    state = {};
  });

  it('should select the initial state', () => {
    expect(selectors.selectQuotationProducts(state)).toEqual(
      initialState.quotationProducts,
    );
  });

  it('should select quotation products', () => {
    const quotationProducts = [];
    state = {
      insuranceCalc: { ...initialState, quotationProducts: [] },
    };
    expect(selectors.selectQuotationProducts(state)).toEqual(quotationProducts);
  });
});
