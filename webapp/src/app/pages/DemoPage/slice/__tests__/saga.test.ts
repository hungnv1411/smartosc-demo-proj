import { PaymentFrequency } from 'models';
import { put, takeLatest } from 'redux-saga/effects';
import * as slice from '..';
import { getProducts, insuranceCalcSaga } from '../saga';

describe('getProducts Saga', () => {
  let quotationProducts: any;
  let getProductsIterator: ReturnType<typeof getProducts>;
  const requestParams = {
    name: 'test',
    genderCd: 'FEMALE',
    dob: '1983-02-21',
    planCode: 'T11A20',
    saPerYear: 30000,
    paymentFrequency: PaymentFrequency.YEARLY,
  };
  const action = {
    payload: requestParams,
    type: slice.insuranceCalcActions.getProductsAction.type,
  };

  beforeEach(() => {
    getProductsIterator = getProducts(action);
  });

  it('should dispatch the getProductsSuccess action if it requests the data successfully', () => {
    quotationProducts = [];

    const requestDescriptor = getProductsIterator.next().value;
    expect(requestDescriptor).toMatchSnapshot();

    const putDescriptor = getProductsIterator.next(quotationProducts).value;
    expect(putDescriptor).toEqual(
      put(slice.insuranceCalcActions.getProductsSuccess(quotationProducts)),
    );
  });

  it('should dispatch the response error', () => {
    const requestDescriptor = getProductsIterator.next().value;
    expect(requestDescriptor).toMatchSnapshot();

    const putDescriptor = getProductsIterator.throw(new Error('some error'))
      .value;
    expect(putDescriptor).toEqual(
      put(slice.insuranceCalcActions.getProductsFailed()),
    );
  });
});

describe('insuranceCalcSaga Saga', () => {
  const insuranceCalcIterator = insuranceCalcSaga();
  it('should start task to watch for getProducts action', () => {
    const takeLatestDescriptor = insuranceCalcIterator.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(
        slice.insuranceCalcActions.getProductsAction.type,
        getProducts,
      ),
    );
  });
});
