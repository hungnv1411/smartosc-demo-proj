import { PaymentFrequency } from 'models';
import * as slice from '..';
import { ContainerState, InsuranceCalcParameters } from '../types';

describe('DemoPage slice', () => {
  let state: ContainerState;

  beforeEach(() => {
    state = slice.initialState;
  });

  it('should return the initial state', () => {
    expect(slice.reducer(undefined, { type: '' })).toEqual(state);
  });

  it('should handle get products', () => {
    const params: InsuranceCalcParameters = {
      name: 'test',
      genderCd: 'FEMALE',
      dob: '1983-02-21',
      planCode: 'T11A20',
      saPerYear: 30000,
      paymentFrequency: PaymentFrequency.YEARLY,
    };
    expect(
      slice.reducer(
        state,
        slice.insuranceCalcActions.getProductsAction(params),
      ),
    ).toEqual<ContainerState>({
      ...slice.initialState,
      quotationProducts: [],
    });
  });

  it('should handle get product success', () => {
    const quotationProducts = [];
    expect(
      slice.reducer(
        state,
        slice.insuranceCalcActions.getProductsSuccess(quotationProducts),
      ),
    ).toEqual<ContainerState>({
      ...slice.initialState,
      quotationProducts,
    });
  });

  it('should handle get product error', () => {
    expect(
      slice.reducer(state, slice.insuranceCalcActions.getProductsFailed()),
    ).toEqual<ContainerState>({
      ...slice.initialState,
    });
  });
});
