import { PaymentFrequency, QuotationProduct } from 'models';

/* --- STATE --- */
export interface InsuranceCalcState {
  quotationProducts: QuotationProduct[];
}

export interface InsuranceCalcParameters {
  name: string;
  genderCd: string;
  dob: string;
  planCode: string;
  paymentFrequency?: PaymentFrequency;
  premiumPerYear?: number;
  saPerYear?: number;
}

export type ContainerState = InsuranceCalcState;
