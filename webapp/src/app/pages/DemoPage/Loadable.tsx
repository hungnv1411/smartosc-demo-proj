/**
 * Asynchronously loads the component for HomePage
 */

import { lazyLoad } from 'utils/loadable';

export const DemoPage = lazyLoad(
  () => import('./index'),
  module => module.DemoPage,
);
