import { Paper, Radio, Typography } from '@material-ui/core';
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Table, { HeaderProps } from 'app/components/Table';
import { QuotationProduct } from 'models';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { currencyFormat } from 'utils/stringFormat';
import { messages } from './messages';
import { SearchBar } from './SearchBar';
import { useInsuranceCalcSlice } from './slice';
import { selectQuotationProducts } from './slice/selectors';
import { InsuranceCalcParameters } from './slice/types';

interface StyleProps {
  smDown?: boolean;
}

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    padding: 16,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    minHeight: 300,
    width: (props: StyleProps) => (props.smDown ? '100%' : '80%'),
    backgroundColor: 'white',
    padding: 16,
  },
  resultTable: {
    marginTop: theme.spacing(3),
  },
}));

interface HeaderPropWrapper extends HeaderProps {
  sortField?: string;
}

export const DemoPage = React.memo(() => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const theme = useTheme();
  const smDown = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles({ smDown });
  const { actions } = useInsuranceCalcSlice();
  const quotationProducts = useSelector(selectQuotationProducts);

  const resultTableHeaders = React.useMemo<HeaderPropWrapper[]>(
    () => [
      { name: '' }, // radio button
      {
        name: t(messages.productId()),
        isAcs: true,
        sortable: true,
        sortField: 'id',
      },
      { name: t(messages.typeCode()) },
      { name: t(messages.familyCode()) },
      { name: t(messages.baseSumAssured()) },
      { name: t(messages.baseAnnualPremium()) },
      { name: t(messages.productTerm()) },
      { name: t(messages.premiumPayingTerm()) },
      // { name: t(messages.payementFrequency()) },
      // { name: t(messages.planCode()) },
    ],
    [t],
  );

  const renderRow = React.useCallback((rowData: QuotationProduct) => {
    return [
      rowData.selected === true ? (
        <Radio checked={rowData.selected} color="primary" />
      ) : null,
      <Typography noWrap>{rowData.productId}</Typography>,
      <Typography noWrap>{rowData.productTypeCd}</Typography>,
      <Typography noWrap>{rowData.productFamilyCd}</Typography>,
      <Typography noWrap>{currencyFormat(rowData.baseSumAssured)}</Typography>,
      <Typography noWrap>
        {currencyFormat(rowData.baseAnnualPremium)}
      </Typography>,
      <Typography noWrap>{rowData.productTerm}</Typography>,
      <Typography noWrap>{rowData.premiumPayingTerm}</Typography>,
      // <Typography noWrap>{rowData.paymentFrequencyCd}</Typography>,
      // <Typography noWrap>{rowData.planCode}</Typography>,
    ];
  }, []);

  const onGetProducts = React.useCallback(
    (params: InsuranceCalcParameters) => {
      dispatch(actions.getProductsAction(params));
    },
    [dispatch, actions],
  );

  return (
    <>
      <Helmet>
        <title>Demo Page</title>
        <meta name="description" content="Demo page" />
      </Helmet>
      <div className={classes.root}>
        <Paper className={classes.content}>
          <SearchBar onCalculateInsurance={onGetProducts} />
          <Table
            className={classes.resultTable}
            headers={resultTableHeaders}
            renderItem={renderRow}
            items={quotationProducts}
          />
        </Paper>
      </div>
    </>
  );
});
