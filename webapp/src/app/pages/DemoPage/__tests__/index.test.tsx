import { Store } from '@reduxjs/toolkit';
import { render } from '@testing-library/react';
import * as React from 'react';
import { HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { configureAppStore } from 'store/configureStore';
import { DemoPage } from '..';
import { initialState, insuranceCalcActions as actions } from '../slice';

function* mockDemoPageSaga() {}

jest.mock('../slice/saga', () => ({
  insuranceCalcSaga: mockDemoPageSaga,
}));

const renderDemoPage = (store: Store) =>
  render(
    <Provider store={store}>
      <HelmetProvider>
        <DemoPage />
      </HelmetProvider>
    </Provider>,
  );

describe('<DemoPage />', () => {
  let store: ReturnType<typeof configureAppStore>;
  let component: ReturnType<typeof renderDemoPage>;

  beforeEach(() => {
    store = configureAppStore();
    component = renderDemoPage(store);
    store.dispatch(actions.getProductsSuccess([]));
    expect(store.getState().insuranceCalc).toEqual(initialState);
  });
  afterEach(() => {
    component.unmount();
  });

  it("shouldn't get product on mount", () => {
    store.dispatch(actions.getProductsSuccess([]));
    component.unmount();
    component = renderDemoPage(store);
    expect(store.getState().insuranceCalc).toEqual(initialState);
  });
});
