import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  productId: () => _t(translations.productId, 'Id'),
  typeCode: () => _t(translations.typeCode, 'Type Code'),
  familyCode: () => _t(translations.familyCode, 'Family Code'),
  baseSumAssured: () => _t(translations.baseSumAssured, 'Base Sum Assured'),
  baseAnnualPremium: () =>
    _t(translations.baseAnnualPremium, 'Base Annual Premium'),
  productTerm: () => _t(translations.productTerm, 'Product Term'),
  premiumPayingTerm: () =>
    _t(translations.premiumPayingTerm, 'Premium Paying Term'),
  payementFrequency: () =>
    _t(translations.payementFrequency, 'Payment Frequency'),
  planCode: () => _t(translations.planCode, 'Plan'),
};
