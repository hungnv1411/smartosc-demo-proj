import { translations } from 'locales/translations';
import { _t } from 'utils/messages';

export const messages = {
  inputInformation: () =>
    _t(translations.inputInformation, 'Input information'),
  nameIsRequired: () => _t(translations.nameIsRequired, 'Name is required'),
  genderIsRequired: () =>
    _t(translations.genderIsRequired, 'Gender is required'),
  dobIsRequired: () => _t(translations.dobIsRequired, 'Dob is required'),
  planIsRequired: () => _t(translations.planIsRequired, 'Plan is required'),
  premiumPerYearIsRequired: () =>
    _t(
      translations.baseAnnualPremiumIsRequired,
      'Premium per year is required',
    ),
  paymentFrequencyIsRequired: () =>
    _t(
      translations.paymentFrequencyIsRequired,
      'Payment frequency is required',
    ),
  sumAssuredPerYearIsRequired: () =>
    _t(
      translations.sumAssuredPerYearIsRequired,
      'Sum assured per year is required',
    ),
  name: () => _t(translations.name, 'Name *'),
  gender: () => _t(translations.gender, 'Gender *'),
  dob: () => _t(translations.dob, 'Dob *'),
  plan: () => _t(translations.plan, 'Plan *'),
  premiumPerYear: () => _t(translations.premiumPerYear, 'Premium per year *'),
  sumAssuredPerYear: () =>
    _t(translations.sumAssuredPerYear, 'Sum assured per year *'),
  paymentFrequency: () =>
    _t(translations.paymentFrequency, 'Payment frequency *'),
  calculate: () => _t(translations.calculate, 'Calculate'),
  calculateSumAssuredByPremium: () =>
    _t(
      translations.calculateSumAssuredByPremium,
      'Calculate sum assured by premium',
    ),
  calculatePremiumBySumAssured: () =>
    _t(
      translations.calculatePremiumBySumAssured,
      'Calculate premium by sum assured',
    ),
};
