import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Autocomplete from 'app/components/Autocomplete';
import Button from 'app/components/Button';
import DateTimePicker from 'app/components/DateTimePicker';
import TextField from 'app/components/TextField';
import format from 'date-fns/format';
import isValid from 'date-fns/isValid';
import isEmpty from 'lodash/isEmpty';
import omit from 'lodash/omit';
import * as models from 'models';
import { PaymentFrequency } from 'models';
import * as React from 'react';
import { useTranslation } from 'react-i18next';
import validation, { ValidationResult } from 'utils/validation';
import { InsuranceCalcParameters } from '../slice/types';
import { messages } from './messages';

const useStyles = makeStyles((theme: Theme) => ({
  inputForm: {
    marginTop: theme.spacing(2),
  },
  calculateButton: {
    marginTop: theme.spacing(3),
  },
}));

interface SearchBarProps {
  onCalculateInsurance: (params: InsuranceCalcParameters) => void;
}

export const SearchBar = React.memo((props: SearchBarProps) => {
  const { t } = useTranslation();
  const classes = useStyles();

  const { onCalculateInsurance } = props;
  const [searchParams, setSearchParams] = React.useState<
    InsuranceCalcParameters
  >({} as InsuranceCalcParameters);
  const [calculateMethod, setCalculateMethod] = React.useState(0);

  const [validationResult, setValidationResult] = React.useState<{
    name?: ValidationResult;
    genderCd?: ValidationResult;
    dob?: ValidationResult;
    planCode?: ValidationResult;
    paymentFrequency?: ValidationResult;
    premiumPerYear?: ValidationResult;
    saPerYear?: ValidationResult;
  }>({});

  const onDobChange = React.useCallback(value => {
    if (isValid(value)) {
      const dob = format(value, 'yyyy-MM-dd');
      setSearchParams(prev => ({
        ...prev,
        dob,
      }));
    } else {
      setSearchParams(prev => omit(prev, 'dob') as InsuranceCalcParameters);
    }
  }, []);

  const onGenderChange = React.useCallback(
    (event, option?: { label: string; value: string }) => {
      setSearchParams(prev => ({
        ...prev,
        genderCd: option?.value || '',
      }));
    },
    [],
  );

  const onPlanChange = React.useCallback(
    (event, option?: { label: string; value: string }) => {
      setSearchParams(prev => ({
        ...prev,
        planCode: option?.value || '',
      }));
    },
    [],
  );

  const onNameChange = React.useCallback(event => {
    const value = event.target.value;
    setSearchParams(prev => ({
      ...prev,
      name: value || '',
    }));
  }, []);

  const onPremiumPerYearChange = React.useCallback(event => {
    const value = event.target.value;
    setSearchParams(prev => ({
      ...prev,
      premiumPerYear: value ? parseInt(value) : 0,
    }));
  }, []);

  const onSumAssuredPerYearChange = React.useCallback(event => {
    const value = event.target.value;
    setSearchParams(prev => ({
      ...prev,
      saPerYear: value ? parseInt(value) : 0,
    }));
  }, []);

  const onFrequencyChange = React.useCallback(
    (event, option?: { label: string; value: PaymentFrequency }) => {
      setSearchParams(prev => ({
        ...prev,
        paymentFrequency: option?.value,
      }));
    },
    [],
  );

  const onCalculateMethodChange = React.useCallback(event => {
    setCalculateMethod(event.target.value);
  }, []);

  const validateFields = () => {
    const validateOption = {
      type: 'empty',
      model: searchParams,
      keys: ['name', 'genderCd', 'dob', 'planCode', 'paymentFrequency'],
      messages: [
        t(messages.nameIsRequired()),
        t(messages.genderIsRequired()),
        t(messages.dobIsRequired()),
        t(messages.planIsRequired()),
        t(messages.paymentFrequencyIsRequired()),
      ],
    };
    if (calculateMethod === 0) {
      validateOption.keys.push('premiumPerYear');
      validateOption.messages.push(t(messages.premiumPerYearIsRequired()));
    } else {
      validateOption.keys.push('saPerYear');
      validateOption.messages.push(t(messages.sumAssuredPerYearIsRequired()));
    }
    const options = [validateOption];
    const result = validation(options);
    setValidationResult(result);
    return isEmpty(result);
  };

  const onCalcButtonClicked = () => {
    if (searchParams && validateFields()) {
      onCalculateInsurance(searchParams);
    }
  };

  return (
    <>
      <Typography>{t(messages.inputInformation())}</Typography>
      <Grid container spacing={3} className={classes.inputForm}>
        <Grid item xs={12} sm={6} md={4}>
          <TextField
            name="username"
            label={t(messages.name())}
            onChange={onNameChange}
            error={validationResult.name?.error}
            helperText={validationResult.name?.helperMessageText}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Autocomplete
            name="gender"
            label={t(messages.gender())}
            options={models.genderOptions}
            getOptionLabel={opt => opt.label}
            onChange={onGenderChange}
            error={validationResult.genderCd?.error}
            helperText={validationResult.genderCd?.helperMessageText}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <DateTimePicker
            name="dob"
            label={t(messages.dob())}
            onChangeDate={onDobChange}
            fullWidth
            defaultDate={null}
            error={validationResult.dob?.error}
            helperText={validationResult.dob?.helperMessageText}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Autocomplete
            name="planCode"
            label={t(messages.plan())}
            options={models.plans}
            getOptionLabel={opt => opt.label}
            onChange={onPlanChange}
            error={validationResult.planCode?.error}
            helperText={validationResult.planCode?.helperMessageText}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Autocomplete
            name="paymentFrequency"
            label={t(messages.paymentFrequency())}
            options={models.paymentFrequency}
            getOptionLabel={opt => opt.label}
            onChange={onFrequencyChange}
            error={validationResult.paymentFrequency?.error}
            helperText={validationResult.paymentFrequency?.helperMessageText}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3} className={classes.inputForm}>
        <Grid item xs={12} sm={6} md={4}>
          <FormControl variant="outlined" fullWidth>
            <Select
              name="calculateMethod"
              value={calculateMethod}
              onChange={onCalculateMethodChange}
              margin="dense"
              fullWidth
            >
              <MenuItem value={0}>
                {t(messages.calculateSumAssuredByPremium())}
              </MenuItem>
              <MenuItem value={1}>
                {t(messages.calculatePremiumBySumAssured())}
              </MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          {calculateMethod === 0 ? (
            <TextField
              name="premiumPerYear"
              label={t(messages.premiumPerYear())}
              onChange={onPremiumPerYearChange}
              type="number"
              error={validationResult.premiumPerYear?.error}
              helperText={validationResult.premiumPerYear?.helperMessageText}
            />
          ) : (
            <TextField
              name="sumAssuredPerYear"
              label={t(messages.sumAssuredPerYear())}
              onChange={onSumAssuredPerYearChange}
              type="number"
              error={validationResult.saPerYear?.error}
              helperText={validationResult.saPerYear?.helperMessageText}
            />
          )}
        </Grid>
      </Grid>
      <Button
        name="calculateButton"
        color="primary"
        className={classes.calculateButton}
        onClick={onCalcButtonClicked}
        disabled={!searchParams}
      >
        {t(messages.calculate())}
      </Button>
    </>
  );
});
