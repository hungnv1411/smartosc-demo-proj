import { fireEvent, render } from '@testing-library/react';
import get from 'lodash/get';
import React from 'react';
import { SearchBar } from '..';
import { InsuranceCalcParameters } from '../../slice/types';

const renderSearchBar = () => {
  const onCalculateInsurance = (params: InsuranceCalcParameters) => {};
  return render(<SearchBar onCalculateInsurance={onCalculateInsurance} />);
};

describe('<SearchBar />', () => {
  let component: ReturnType<typeof renderSearchBar>;

  beforeEach(() => {
    component = renderSearchBar();
  });
  afterEach(() => {
    component.unmount();
  });

  it('should change username field value on action', () => {
    const value = 'test';
    const form = renderSearchBar();

    const input = form.container.querySelector('[name=username]');
    fireEvent.change(input!, { target: { value: value } });

    expect(get(form.container.querySelector('[name=username]'), 'value')).toBe(
      value,
    );
  });
});
