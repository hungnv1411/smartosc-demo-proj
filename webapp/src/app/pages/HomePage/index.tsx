import { makeStyles, Theme } from '@material-ui/core/styles';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    height: '100vh',
    backgroundImage:
      "url('https://www.travelphotospirit.com/wp-content/uploads/2019/01/71A0333-Modifica.jpg')",
    backgroundSize: 'cover',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

export function HomePage() {
  const classes = useStyles();

  return (
    <>
      <Helmet>
        <title>Home Page</title>
        <meta name="description" content="Home page" />
      </Helmet>
      <div className={classes.root}></div>
    </>
  );
}
