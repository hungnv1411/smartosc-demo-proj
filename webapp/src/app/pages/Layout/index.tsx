import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Footer } from './Footer';
import { Header } from './Header';
import { AppContent, MainSinglePage, Root } from './styles';
interface Props {
  children: React.ReactNode;
}

export function Layout(props: Props) {
  // const dispatch = useDispatch();

  const { children } = props;

  return (
    <>
      <Helmet>
        <title>Demo</title>
        <meta name="description" content="Description of Layouts" />
      </Helmet>
      <Root>
        <AppContent>
          <Header />
          <MainSinglePage>{children}</MainSinglePage>
          <Footer />
        </AppContent>
      </Root>
    </>
  );
}
