import MuiPaper from '@material-ui/core/Paper';
import { spacing } from '@material-ui/system';
import styled from 'styled-components';

const Root = styled.div`
  display: flex;
  min-height: 100vh;
`;

const AppContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  background: ${props => props.theme.body.background};
  width: 0;
  overflow: auto;
`;

const Paper = styled(MuiPaper)(spacing);

const MainContent = styled(Paper)`
  flex: 1;
  background: ${props => props.theme.body.background};
  padding: 0;
  border-radius: 0;
  .MuiPaper-root {
    .MuiPaper-root {
      border: 0;
    }
  }
`;

const MainSinglePage = styled.div`
  flex: 1;
`;

export { Root, AppContent, MainContent, MainSinglePage };
