import { Typography } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import React from 'react';
import { backgroundColor, textColor } from 'styles/colors';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: 100,
    backgroundColor: backgroundColor[2],
    paddingLeft: theme.spacing(8),
    display: 'flex',
    alignItems: 'center',
  },
  footerNote: {
    color: textColor[0],
  },
}));

export const Footer = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography className={classes.footerNote}>
        @ This is footer content
      </Typography>
    </div>
  );
};
