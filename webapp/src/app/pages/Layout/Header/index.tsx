import { Grid, Hidden, IconButton } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { MenuOpenOutlined } from '@material-ui/icons';
import Button from 'app/components/Button';
import AppIcon from 'app/components/icons/AppIcon';
import clsx from 'clsx';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useLocation } from 'react-router';
import { paths } from 'routes/paths';
import { messages } from '../messages';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: 'white',
    height: 100,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  logo: {
    width: 64,
    height: 64,
    marginLeft: theme.spacing(8),
  },
  menuIcon: {
    marginLeft: theme.spacing(8),
  },
  menuItems: {
    margin: 0,
    paddingLeft: theme.spacing(8),
    width: '100% !important',
  },
  menuButton: {
    height: 32,
    width: 100,
  },
  activeButton: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
  },
  normalButton: {
    backgroundColor: 'white',
    color: 'gray',
    border: '1px solid gray',
  },
}));

export const Header = () => {
  const classes = useStyles();
  const { t } = useTranslation();

  const history = useHistory();
  const location = useLocation();

  const onNavigateHomePage = React.useCallback(() => {
    history.push(paths.home);
  }, [history]);

  const onNavigateDemoPage = React.useCallback(() => {
    history.push(paths.demo);
  }, [history]);

  return (
    <div className={classes.root}>
      <Hidden smUp>
        <IconButton className={classes.menuIcon}>
          <MenuOpenOutlined color="primary" />
        </IconButton>
      </Hidden>
      <AppIcon className={classes.logo} width={64} height={64} />
      <Hidden xsDown>
        <Grid container spacing={2} className={classes.menuItems}>
          <Grid item>
            <Button
              onClick={onNavigateHomePage}
              className={clsx(classes.menuButton, {
                [classes.activeButton]:
                  location.pathname.indexOf(paths.home) !== -1,
                [classes.normalButton]:
                  location.pathname.indexOf(paths.home) === -1,
              })}
            >
              {t(messages.home())}
            </Button>
          </Grid>
          <Grid item>
            <Button
              onClick={onNavigateDemoPage}
              className={clsx(classes.menuButton, {
                [classes.activeButton]:
                  location.pathname.indexOf(paths.demo) !== -1,
                [classes.normalButton]:
                  location.pathname.indexOf(paths.demo) === -1,
              })}
            >
              {t(messages.demo())}
            </Button>
          </Grid>
        </Grid>
      </Hidden>
    </div>
  );
};
