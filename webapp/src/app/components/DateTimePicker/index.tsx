import DateFnsUtils from '@date-io/date-fns';
import { makeStyles } from '@material-ui/core/styles';
import {
  KeyboardDatePicker,
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import React, { memo, useCallback, useState } from 'react';

const useStyles = makeStyles(() => ({
  datePicker: {
    marginTop: 0,
    marginBottom: 0,
  },
  inputDatePicker: {},
}));
interface Props {
  defaultDate?: Date | number | null;
  label?: string;
  fullWidth?: boolean;
  showTimePicker?: boolean;
  onChangeDate: (value?: Date | null) => void;
  error?: boolean;
  helperText?: string;
  name?: string;
}

function DatePicker(props: Props) {
  const {
    defaultDate,
    label,
    fullWidth,
    showTimePicker = false,
    onChangeDate,
    error,
    helperText,
    name,
  } = props;
  const classes = useStyles();

  const [selectedDate, setSelectedDate] = useState(defaultDate);

  const handleDateChange = useCallback(
    newDate => {
      setSelectedDate(newDate);
      onChangeDate(newDate);
    },
    [onChangeDate],
  );

  // useEffect(() => {
  //   if (defaultDate && !selectedDate) {
  //     setSelectedDate(defaultDate);
  //   }
  // }, [defaultDate]);

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      {showTimePicker ? (
        <KeyboardDateTimePicker
          inputVariant="outlined"
          className={classes.datePicker}
          variant="inline"
          format="yyyy/MM/dd HH:mm:ss"
          margin="dense"
          label={label}
          placeholder="____/__/__ __:__:__"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{ size: 'small' }}
          InputProps={{ className: classes.inputDatePicker }}
          fullWidth={fullWidth}
          error={error}
          helperText={helperText}
          name={name}
        />
      ) : (
        <KeyboardDatePicker
          inputVariant="outlined"
          className={classes.datePicker}
          disableToolbar
          variant="inline"
          format="yyyy/MM/dd"
          margin="dense"
          label={label}
          placeholder="____/__/__"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{ size: 'small' }}
          InputProps={{ className: classes.inputDatePicker }}
          fullWidth={fullWidth}
          error={error}
          helperText={helperText}
          name={name}
        />
      )}
    </MuiPickersUtilsProvider>
  );
}

export default memo(DatePicker);
