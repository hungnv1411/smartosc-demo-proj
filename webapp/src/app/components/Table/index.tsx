import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import MuiTable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import forEach from 'lodash/forEach';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import React, { useEffect, useState } from 'react';
import { backgroundColor } from 'styles/colors';
import TablePaginationActions from './pagination-actions';

const NUMBER_PAGE_SIZE = 10;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: 'white',
      width: '100%',
      overflow: 'hidden',
    },
    table: {},
    emptyRow: {
      height: 100,
    },
    paging: {
      border: 'none',
    },
    headerCell: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'nowrap',
    },
    tableRow: {
      '&:hover': {
        backgroundColor: backgroundColor[1],
      },
      '&:hover a': {
        fontWeight: 'bold',
      },
    },
  }),
);

export interface HeaderProps {
  name?: string;
  item?: any; // component
  align?: 'inherit' | 'left' | 'center' | 'right' | 'justify';
  width?: number;
  isAcs?: boolean;
  sortable?: boolean;
  icon?: React.ReactNode;
}

interface Props {
  headers: HeaderProps[];
  items?: any[];
  totalElements?: number;
  renderItem: (item: any, index?: number) => any[];
  fetchDataForPage?: (page: number, limit: number) => void;
  onSelectRow?: (rowData: any) => void;
  limitElement?: number;
  pageNumber?: number;
  sortByColumn?: (headerName: string, isAsc: boolean) => void;
  className?: string;
}

export default function Table(props: Props) {
  const classes = useStyles();
  const {
    headers,
    items,
    renderItem,
    totalElements = 0,
    fetchDataForPage,
    onSelectRow,
    limitElement,
    pageNumber,
    sortByColumn,
    className = '',
  } = props;

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(NUMBER_PAGE_SIZE);
  const [localHeaders, setLocalHeaders] = useState<HeaderProps[]>(headers);

  React.useEffect(() => {
    setLocalHeaders(prev => {
      const ret: HeaderProps[] = [];
      for (let i = 0; i < headers.length; i += 1) {
        const obj: HeaderProps = {
          ...headers[i],
          isAcs: prev[i].isAcs,
        };
        ret.push(obj);
      }
      return ret;
    });
  }, [headers]);

  useEffect(() => {
    if (limitElement) {
      setRowsPerPage(limitElement);
    }
  }, [limitElement]);

  useEffect(() => {
    setPage(pageNumber || 0);
  }, [pageNumber]);

  const handleChangePage = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number,
  ) => {
    if (fetchDataForPage) {
      setPage(newPage);
      fetchDataForPage(newPage, rowsPerPage);
    }
  };

  const handleSelectRow = (rowData: any) => {
    if (onSelectRow) {
      onSelectRow(rowData);
    }
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const newRowPerPage = parseInt(event.target.value, 10);
    setRowsPerPage(newRowPerPage);
    if (fetchDataForPage) {
      setPage(0);
      fetchDataForPage(0, newRowPerPage);
    }
  };

  const handleSortDirectionChange = (header: HeaderProps) => {
    if (sortByColumn && header.name) {
      sortByColumn(header.name, !header.isAcs);
    }
    const headerArr = [...localHeaders];
    forEach(headerArr, item => {
      if (item.name === header.name) {
        item.isAcs = !item.isAcs;
      }
    });
    setLocalHeaders(headerArr);
  };

  return (
    <Paper className={`${className} ${classes.root}`} elevation={0}>
      <TableContainer className={classes.table}>
        <MuiTable
          aria-labelledby="tableTitle"
          size="medium"
          aria-label="enhanced table"
        >
          <TableHead>
            <TableRow>
              {localHeaders.map((header, index) => (
                <TableCell
                  key={header.name || index}
                  align={header.align}
                  style={{ width: headers[index].width }}
                  sortDirection="asc"
                >
                  <div className={classes.headerCell}>
                    {header.icon && <span>{header.icon}</span>}
                    <TableSortLabel
                      active={header.sortable}
                      direction={header.isAcs ? 'asc' : 'desc'}
                      style={{ whiteSpace: 'nowrap' }}
                      onClick={() => handleSortDirectionChange(header)}
                      hideSortIcon={!header.sortable}
                    >
                      {header.item ? header.item : header.name}
                    </TableSortLabel>
                  </div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {isEmpty(items) ? (
              <TableRow className={classes.emptyRow}>
                <TableCell align="center" colSpan={headers.length}>
                  {isUndefined(items) ? (
                    <CircularProgress color="primary" />
                  ) : (
                    <Typography>No records to display</Typography>
                  )}
                </TableCell>
              </TableRow>
            ) : (
              items?.map((item, idx) => (
                <TableRow
                  key={idx}
                  onClick={() => handleSelectRow(item)}
                  className={classes.tableRow}
                >
                  {renderItem(item, idx).map((col, index) => (
                    <TableCell
                      key={(col && col.key) || `${idx}-${index}`}
                      align={headers[index].align}
                      style={{ width: headers[index].width }}
                    >
                      {col}
                    </TableCell>
                  ))}
                </TableRow>
              ))
            )}
          </TableBody>
          {totalElements > 0 && fetchDataForPage && (
            <TableFooter>
              <TableRow>
                <TablePagination
                  className={classes.paging}
                  rowsPerPageOptions={[5, 10, 25, 50]}
                  count={totalElements || 1}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          )}
        </MuiTable>
      </TableContainer>
    </Paper>
  );
}
