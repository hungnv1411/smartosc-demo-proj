import { makeStyles, Theme } from '@material-ui/core/styles';
import MuiTextField, { TextFieldProps } from '@material-ui/core/TextField';
import React from 'react';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: 'white',
    marginTop: 0,
    marginBottom: 0,
  },
}));

const TextField = (props: TextFieldProps) => {
  const classes = useStyles();
  return (
    <MuiTextField
      {...props}
      margin="dense"
      className={classes.root}
      variant="outlined"
      fullWidth
    />
  );
};

export default React.memo(TextField);
