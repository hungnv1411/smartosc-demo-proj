import MuiButton, { ButtonProps } from '@material-ui/core/Button';
import { makeStyles, Theme } from '@material-ui/core/styles';
import React from 'react';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: 32,
    minWidth: 80,
  },
}));

const Button = (props: ButtonProps) => {
  const { variant = 'contained', children, className = '', ...rest } = props;
  const classes = useStyles();
  return (
    <MuiButton
      variant={variant}
      disableElevation
      className={`${classes.root} ${className}`}
      {...rest}
    >
      {children}
    </MuiButton>
  );
};

export default Button;
