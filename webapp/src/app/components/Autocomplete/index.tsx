import { TextField } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import {
  Autocomplete as MuiAutocomplete,
  UseAutocompleteProps,
} from '@material-ui/lab';
import React from 'react';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      margin: 0,
      padding: 0,
    },
  }),
);

interface Props<T> extends UseAutocompleteProps<T, boolean, boolean, boolean> {
  options: any[];
  getOptionLabel: (object: any) => string;
  className?: any;
  label?: string;
  placeholder?: string;
  size?: 'small' | 'medium';
  [propName: string]: any;
}
function Autocomplete(props: Props<any>) {
  const classes = useStyles();
  const {
    className,
    label,
    error,
    helperText,
    placeholder,
    name,
    ...rest
  } = props;

  return (
    <MuiAutocomplete
      {...rest}
      className={`${classes.root} ${className}`}
      id={name}
      // debug
      renderInput={params => (
        <TextField
          {...params}
          label={label}
          placeholder={placeholder}
          margin="dense"
          variant="outlined"
          fullWidth
          style={{ margin: 0 }}
          error={error}
          helperText={helperText}
          name={`autocomplete-input-${name}`}
        />
      )}
    />
  );
}

export default Autocomplete;
