/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import { MuiThemeProvider } from '@material-ui/core';
import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { useTranslation } from 'react-i18next';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { GlobalStyle } from 'styles/global-styles';
import Routes from '../routes';
import { getMuiTheme } from '../styles/theme';
export function App() {
  const { i18n } = useTranslation();
  const theme = getMuiTheme();
  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - Demo"
        defaultTitle="Demo"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="Demo" />
      </Helmet>
      <ThemeProvider theme={theme}>
        <MuiThemeProvider theme={theme}>
          <Routes />
        </MuiThemeProvider>
        <GlobalStyle />
      </ThemeProvider>
    </BrowserRouter>
  );
}
