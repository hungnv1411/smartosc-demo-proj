import { PaletteOptions } from '@material-ui/core/styles/createPalette';

const palette: PaletteOptions = {
  type: 'light',
  primary: {
    main: '#fe5a3a',
    contrastText: '#FFF',
  },
  secondary: {
    main: 'rgba(0, 0, 0, 0.04)',
    contrastText: '#FFF',
  },
};

export default palette;
