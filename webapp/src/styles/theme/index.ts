import { createMuiTheme } from '@material-ui/core/styles';
import { SpacingOptions } from '@material-ui/core/styles/createSpacing';
import customTheme, { IBody, IHeader } from './custom-theme';
import palette from './palette';
import typography from './typography';

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    header: IHeader;
    body: IBody;
  }

  interface ThemeOptions {
    header?: IHeader;
    body?: IBody;
  }
}

export const getMuiTheme = () =>
  createMuiTheme({
    spacing: 4 as SpacingOptions,
    palette,
    typography,
    header: customTheme.header,
    body: customTheme.body,
  });
