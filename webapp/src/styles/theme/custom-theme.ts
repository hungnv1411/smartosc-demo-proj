import { backgroundColor } from 'styles/colors';

export interface IHeader {
  color: string;
  background: string;
  search: {
    color: string;
  };
  indicator: {
    background: string;
  };
}

export interface IBody {
  background: string;
}

interface ICustomTheme {
  name: string;
  header: IHeader;
  body: IBody;
}

const customTheme: ICustomTheme = {
  name: 'Light',
  header: {
    color: '#EA5C5F',
    background: '#FFF',
    search: {
      color: '#FDF4F4',
    },
    indicator: {
      background: '#FFF',
    },
  },
  body: {
    background: backgroundColor[1],
  },
};

export default customTheme;
