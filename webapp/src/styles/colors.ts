// ##############################
// // // Function that converts from hex color to rgb color
// // // Example: input = #9c27b0 => output = 156, 39, 176
// // // Example: input = 9c27b0 => output = 156, 39, 176
// // // Example: input = #999 => output = 153, 153, 153
// // // Example: input = 999 => output = 153, 153, 153
// #############################
const hexToRgb = (strHex: string) => {
  let input = strHex;
  input = input.replace('#', '');
  const hexRegex = /[0-9A-Fa-f]/g;
  if (!hexRegex.test(input) || (input.length !== 3 && input.length !== 6)) {
    throw new Error('input is not a valid hex color.');
  }
  if (input.length === 3) {
    const first = input[0];
    const second = input[1];
    const last = input[2];
    input = first + first + second + second + last + last;
  }
  input = input.toUpperCase();
  const first = input[0] + input[1];
  const second = input[2] + input[3];
  const last = input[4] + input[5];
  return `${parseInt(first, 16)}, ${parseInt(second, 16)}, ${parseInt(
    last,
    16,
  )}`;
};

const primaryColor = '#597EF7';
const secondaryColor = '#E3DCFF';
const warningColor = ['#ff9800'];
const dangerColor = ['#f44336'];
const successColor = ['#4caf50'];
const borderColor = ['#F0F1F5', '#dddddd', '#eeeeee', '#ffffff'];
const backgroundColor = ['#FFF', '#f2f2f2', '#e6e7e8'];
const textColor = ['#414141'];

export {
  hexToRgb,
  primaryColor,
  secondaryColor,
  warningColor,
  dangerColor,
  successColor,
  borderColor,
  backgroundColor,
  textColor,
};
