process.env['NODE_CONFIG_DIR'] = __dirname + '/configs';

import App from '@/app';
import validateEnv from '@utils/validateEnv';
import 'dotenv/config';
import ProductsRoute from './routes/products.route';

validateEnv();

const app = new App([new ProductsRoute()]);

app.listen();
