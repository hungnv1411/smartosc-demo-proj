import axios, { AxiosInstance } from 'axios';

const baseConfig = (
  baseURL?: string,
  contentType: string = 'application/json',
) => {
  return {
    baseURL: baseURL,
    headers: {
      'Accept-Language': 'en-US',
      'Content-Type': contentType,
    },
  };
};

export const createService = (
  baseURL?: string,
  contentType: string = 'application/json',
): AxiosInstance => {
  return axios.create(baseConfig(baseURL, contentType));
};

export const productsService = createService(process.env.ECOMM_API);
