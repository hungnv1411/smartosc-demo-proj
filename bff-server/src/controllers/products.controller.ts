import { productsService } from '@services/products.service';
import { AxiosResponse } from 'axios';
import { NextFunction, Request, Response } from 'express';

class ProductsController {
  public getProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const calculationResult: AxiosResponse<{ quotationProductList: any }> = await productsService.post('/getProduct', req.body);
      const quotationProducts = calculationResult.data.quotationProductList || [];
      res.status(200).json({ data: quotationProducts, message: 'getProduct' });
    } catch (error) {
      next(error);
    }
  };
}

export default ProductsController;
