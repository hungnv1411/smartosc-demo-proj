import ProductsController from '@controllers/products.controller';
import { Routes } from '@interfaces/routes.interface';
import { Router } from 'express';

class ProductsRoute implements Routes {
  public path = '/api/v1/products';
  public router = Router();
  public productsController = new ProductsController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/getProduct`, this.productsController.getProduct);
  }
}

export default ProductsRoute;
