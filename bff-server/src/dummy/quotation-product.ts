
export const DummyProduct = {
  productId: 'ECOMMBIG3',
  productTypeCd: 'PLAN',
  productFamilyCd: 'TERM',
  baseSumAssured: 1205594,
  baseAnnualPremium: 30000,
  productTerm: 5,
  premiumPayingTerm: 5,
  paymentFrequencyCd: 'YEARLY',
  planCode: 'T11A20',
  selected: false,
}